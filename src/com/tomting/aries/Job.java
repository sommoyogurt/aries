package com.tomting.aries;

import java.util.concurrent.Semaphore;

import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;

public class Job {
	public ThrfSrvc service;
	public ThrfSrvr serviceResult;
	public Semaphore lock;
	public boolean valid;
	
	
	public Job (ThrfSrvc service) {
		
		this.service = service;
		this.serviceResult = null;
		this.valid = true;
		this.lock = new Semaphore (0);
	}
	
	public Job () {
		
		this.valid = false;
	}
	
}
