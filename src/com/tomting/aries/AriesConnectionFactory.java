package com.tomting.aries;

import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnectionFactory;

public class AriesConnectionFactory implements OCFI {
	OrionConnectionFactory ocf;
	AriesConnection oc;
	int workers;
	int bulkSize;
	
	public AriesConnectionFactory (OrionConnectionFactory ocf, int workers, int bulkSize) {
		
		this.ocf = ocf;
		this.workers = workers;
		this.bulkSize = bulkSize;
		oc = new AriesConnection (ocf, workers, bulkSize).addRef();
	}
	
	@Override
	public void close () {
		
		oc.subRef();
	}
	
	@Override
	public void checkIn(OCI oc) {

	}

	@Override
	public OCI checkOut() {

		return oc;
	}

	@Override	
	public String getHost () {
		
		return ocf.getHost ();
	}

	@Override	
	public int getPortIO () {
		
		return ocf.getPortIO ();
	}

	@Override	
	public int getPortNIO () {
		
		return ocf.getPortNIO ();
	}		

}
