package com.tomting.aries;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;

import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.ThrfL2os;
import com.tomting.orion.ThrfL2qb;
import com.tomting.orion.ThrfL2qr;
import com.tomting.orion.ThrfL2st;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.iEservicetype;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnectionFactory;

public class AriesConnection  implements OCI {
	OrionConnectionFactory ocf;
	List<Worker> workerPool = new ArrayList<Worker> ();
	int simpleRule = 0;
	int workers;
	int bulkSize;
	int subCounter = 0;
	int refCount = 0;
	String namespace;
	
	public AriesConnection (OrionConnectionFactory ocf, int workers, int bulkSize) {
		
		this.workers = workers;
		this.bulkSize = bulkSize;
		this.ocf = ocf;
		this.namespace = ocf.getNamespace();
	}
	
	/**
	 * add reference
	 */
	public AriesConnection addRef () {
		
		synchronized (this) {
			refCount++;
			if (refCount == 1) {
				for (int i = 0; i < workers; i++) {
					Worker worker = new Worker (ocf, bulkSize);
					worker.setDaemon(true);
					worker.start();
					workerPool.add(worker);
				}				
			}
		}
		return this;
	}
	
	/**
	 * remove reference
	 */
	public AriesConnection subRef () {
		
		synchronized (this) {
			refCount--;
			if (refCount == 0) {
				for (Worker w : workerPool) {
					try {
						w.abort();
						w.join();
					} catch (InterruptedException e) {}			
				}				
			}
		}
		return this;
	}
	
	/**
	 * add job
	 * @param job
	 */
	public void addJob (Job job) {
		int index;
		
		synchronized (this) {
			subCounter = ++subCounter % bulkSize;	
			if (subCounter == 0) index = ++simpleRule % workers;
			else index = simpleRule % workers;
		}
		workerPool.get(index).addJob(job);
	}
	
	@Override
	public List<ThrfL2ks> runOsql(ThrfL2os osql) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		
		service.cVosql = osql;
		service.cVosql.sVnamespace = namespace;
		service.iVservicetype = iEservicetype.OSQL;
		Job job = new Job (service);
		addJob(job);
		try {
			job.lock.acquire();
		} catch (InterruptedException e) {}
		return job.serviceResult.cVdmlresult;
	}

	@Override
	public ThrfL2qb runQuery(ThrfL2qr query) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		
		service.cVquery = query;
		service.cVquery.cVmutable.sVnamespace = namespace;		
		service.iVservicetype = iEservicetype.QUERY;
		Job job = new Job (service);
		addJob(job);
		try {
			job.lock.acquire();
		} catch (InterruptedException e) {}
		return job.serviceResult.cVqueryresult;
	}

	@Override
	public boolean runStatement(ThrfL2st statement) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		
		service.cVstatement = statement;
		service.cVstatement.cVmutable.sVnamespace = namespace;	
		service.cVstatement.cVkey.iVtimestamp = com.tomting.orion.connection.Helpers.getTimestamp();
		service.iVservicetype = iEservicetype.STATEMENT;
		Job job = new Job (service);
		addJob(job);
		try {
			job.lock.acquire();
		} catch (InterruptedException e) {}
		return job.serviceResult.bVreturn;
	}
	
	@Override
	public void runStatementFast (ThrfL2st statement) throws TException {
		
		runStatement (statement);
	}	

	@Override
	public void close() throws TException {
		
		subRef ();
	}

	@Override
	public boolean isOpen() {
		boolean result;
		
		synchronized (this) {
			result = refCount > 0;
		}
		return result;
	}
	
}
