package com.tomting.aries;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.thrift.TException;

import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnection;
import com.tomting.orion.connection.OrionConnectionFactory;

public class Worker extends Thread {	
	OrionConnectionFactory ocf;
	BlockingQueue<Job> queue = new LinkedBlockingQueue<Job> ();
	int bulkSize;
	
	public Worker (OrionConnectionFactory ocf, int bulkSize) {
		this.ocf = ocf;
		this.bulkSize = bulkSize;
	}
	
	public void addJob (Job job) {
		
		try {
			queue.put(job);
		} catch (InterruptedException e) {}
	}
	
	@Override
	public void run () {
	
		while (true) {
			try {
				List<Job> bulk = new ArrayList<Job> ();
				List<ThrfSrvc> batch = new ArrayList<ThrfSrvc> ();
				List<ThrfSrvr> batchResult = null;
				Job job = queue.take();
				if (job.valid) {
					bulk.add(job);
					batch.add(job.service);
				}				
				int bulkDim = Math.min(queue.size(), bulkSize);
				while (job.valid && bulkDim-- > 0) {
					job = queue.take();
					if (job.valid) {
						bulk.add(job);
						batch.add(job.service);
					}
				}
				if (batch.size() > 0) {
			        OCI orion = ocf.checkOut();        
			        try {
			        	batchResult = ((OrionConnection) orion).runThriftbatch(batch);  
					} catch (TException e) {	
						e.printStackTrace();
					} finally {
						ocf.checkIn(orion);
					}   	
			        for (int i = 0; i < bulk.size();i++) {
			        	try {
			        		bulk.get(i).serviceResult = batchResult.get(i);
			        	} catch (Exception e) {}
			        	bulk.get(i).lock.release();
			        }	
				}
				if (!job.valid) break;
			} catch (InterruptedException e) {}
			
		}
	}
	
	public void abort () {
		queue.add(new Job ());
	}
	
}
